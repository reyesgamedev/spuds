package db;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteAccessPermException;
import android.database.sqlite.SQLiteCantOpenDatabaseException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
import android.widget.Toast;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Created by Edgar
 */
public class DatabaseHelper extends SQLiteOpenHelper{

    private static final int BUFFER_SIZE = 4096;
    private static final String TAG = "DOWNLOAD";
    private static final String QUERY_VERSION = "SELECT * FROM version";
    private static String DB_NAME = "spud.sqlite";
    private static String SERVER = "http://gamedev.x10host.com/update.php?version=";

    private String dir = "";

    private SQLiteDatabase db;

    private final Context context;

    private String version = "0";

    public DatabaseHelper(Context context) {
        super(context, DB_NAME,null,1);
        this.context = context;
        this.dir = context.getFilesDir().toString() + "/";
        // check if file exist
        if(databseExists())
        {
            open();
        }
    }


    public boolean download(){





        URL url;
        HttpURLConnection con;

        try{
            url = new URL(SERVER + version);

            con = (HttpURLConnection) url.openConnection();

            int responseCode = con.getResponseCode();

            if(responseCode != HttpURLConnection.HTTP_OK){
                Log.v(TAG,"HTTP Erro Code: " + responseCode);
                return false;
            }

            String fileName = "";

            String disposition = con.getHeaderField("Content-Disposition");
            String contentType = con.getContentType();
            int contentLength = con.getContentLength();






            if(disposition != null) {
                int index = disposition.indexOf("filename=");
                if(index > 0) {
                    fileName = disposition.substring(index + 10, disposition.length() - 1);
                }
            }else {
                String page = url.toString();

                fileName = "spud.sqlite";
            }

                Log.v(TAG, "Content-Type = " + contentType);
                Log.v(TAG, "Content-Disposition = " + disposition);
                Log.v(TAG, "Content-Length = " + contentLength);
                Log.v(TAG, "fileName = " + fileName);


                InputStream inputStream = con.getInputStream();
                String saveFilePath = dir + DB_NAME;

                FileOutputStream outputStream = new FileOutputStream(saveFilePath);

                int bytesRead = -1;
                byte[] buffer = new byte[BUFFER_SIZE];

                while((bytesRead = inputStream.read(buffer)) != -1){
                    outputStream.write(buffer, 0, bytesRead);
                }

                outputStream.close();
                inputStream.close();
                SQLiteDatabase db = SQLiteDatabase.openDatabase(dir+DB_NAME,null,SQLiteDatabase.OPEN_READONLY | SQLiteDatabase.NO_LOCALIZED_COLLATORS);
                Cursor res = db.rawQuery("SELECT * FROM version", null);

                res.moveToFirst();

                version = res.getString(res.getColumnIndex("version_number"));
                Toast.makeText(context, version,Toast.LENGTH_LONG).show();


                Log.v(TAG, "File Downloaded");

            con.disconnect();
        }catch(IOException e){
            e.printStackTrace();
        }catch (SQLiteException e){
            e.printStackTrace();
        }

        return true;
    }

    public boolean databseExists(){
        File db = new File(dir, DB_NAME);

        if(db.exists()){
            return true;
        }

        return false;
    }

    public void open()
    {
        if(!databseExists()){
            return;
        }

        try {
            db = SQLiteDatabase.openDatabase(dir+ DB_NAME, null,
                    SQLiteDatabase.OPEN_READONLY | SQLiteDatabase.NO_LOCALIZED_COLLATORS);

            Cursor res = db.rawQuery("SELECT * FROM version", null);

            res.moveToFirst();

            version = res.getString(res.getColumnIndex("version_number"));
            res.close();
            Toast.makeText(context, version,Toast.LENGTH_LONG).show();
        } catch (SQLiteException e) {
            e.printStackTrace();
        }

    }
    @Override
    public synchronized void close(){
        if(db != null){
            db.close();
        }
        super.close();
    }
    @Override
    public void onCreate(SQLiteDatabase db) {

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    public Cursor spuds(String type){
        Cursor res = db.rawQuery("SELECT * FROM spud WHERE spud_type_name=?",new String[]{type});

        return res;
    }

    public Cursor getAddition(String type){
        Cursor res = db.rawQuery("SELECT * FROM additions WHERE additions_addition_type=?", new String[]{type});
        return res;
    }

    public String getVersion() {
        return version;
    }
}
