package db;

/**
 * Created by Edgar.
 */

public class Order {
    private int id;
    private String name;
    private double price;
    private int proteins[];
    private int pLimit;
    private int cheeses[];
    private int cLimit;
    private int vegetables[];
    private int vLimit;
    private int toppings[];
    private int tLimit;
    private int suacesAndCreams[];
    private int sacLimit;

    public Order(int id , String name, double price){
        this.id = id;
        this.name = name;
        this.price = price;
    }

    public void setLimits(int protein, int cheese, int vegetables, int toppings, int saucesAndCreams)
    {
        pLimit = protein;
        cLimit = cheese;
        vLimit = vegetables;
        tLimit = toppings;
        sacLimit = saucesAndCreams;
    }

}
