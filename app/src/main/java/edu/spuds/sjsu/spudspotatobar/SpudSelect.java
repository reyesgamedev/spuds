package edu.spuds.sjsu.spudspotatobar;

import android.app.ActionBar;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.text.Layout;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ScrollView;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.List;

import db.DatabaseHelper;

public class SpudSelect extends AppCompatActivity {
    private MyCustomAdapter adapterList;
    private ListView listView;
    private ArrayList<Item> items = new ArrayList<Item>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_spud_select);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        // getdata
        DatabaseHelper db = new DatabaseHelper(this);
        db.open();

        items.add(new Item(R.layout.spud_type, "<b>Signature Potatoes</b>"));

        Cursor sig = db.spuds("Signature Spud");

        while(sig.moveToNext())
        {
            String name= sig.getString(sig.getColumnIndex("spud_name"));
            String price = sig.getString(sig.getColumnIndex("spud_price"));
            byte[] imageBlob = sig.getBlob(sig.getColumnIndex("spud_image"));
            items.add(new Item(
                    R.layout.spud_list,
                    name,
                    price,
                    imageBlob));
        }

        items.add(new Item(R.layout.spud_type, "<b>Build your Own Spuds</b>"));

        // get from database



        adapterList = new MyCustomAdapter(this, items);

        listView = (ListView) findViewById(R.id.spud_listView);

        listView.setAdapter(adapterList);



    }

}
