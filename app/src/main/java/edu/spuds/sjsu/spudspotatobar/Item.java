package edu.spuds.sjsu.spudspotatobar;


import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

public class Item
{
    private String text;
    private String price;
    private Bitmap drawable;
    private int layoutID;


    public Item(int layoutID, String text, String price, byte[] imageBlob) {
        this.layoutID = layoutID;
        this.text = text;
        this.price = price;
        this.drawable = BitmapFactory.decodeByteArray(imageBlob,0,imageBlob.length);

    }

    public Item(int layoutID, String text) {
        this.text = text;
        this.layoutID = layoutID;
    }
    // getters
    public String getText(){return text;}
    public String getPrice(){return price;}
    public Bitmap getDrawable(){return drawable;}
    public int getLayoutID(){return layoutID;}
}
