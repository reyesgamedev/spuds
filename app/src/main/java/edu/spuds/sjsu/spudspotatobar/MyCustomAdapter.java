package edu.spuds.sjsu.spudspotatobar;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

public class MyCustomAdapter extends BaseAdapter{

    private ArrayList<Item> dataList;

    private LayoutInflater inflater;



    private Context context;
    private int position;

    public MyCustomAdapter(Context context, ArrayList<Item> list)
    {
        dataList = list;
        this.inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.context = context;

    }


    @Override
    public int getCount() {
        return dataList.size();
    }

    @Override
    public Object getItem(int position) {
        return dataList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;
        ViewHolder viewHolder;
        Item item = dataList.get(position);

        LayoutInflater li = inflater;

        v = li.inflate(item.getLayoutID(),null);
        viewHolder = new ViewHolder(v);


        viewHolder.text.setText(Html.fromHtml(item.getText()));

        if(item.getLayoutID() == R.layout.spud_list) {

            viewHolder.price.setText(Html.fromHtml(item.getPrice()));
            int maxWidth = 100;
            int maxHeight = 100;
            // scale image
            int width = item.getDrawable().getWidth();
            int height = item.getDrawable().getHeight();

            float ratio = 0;

            if(width > maxWidth) {
                ratio = (float)maxWidth/width;
                height = (int)(height * ratio);
                width = (int)(width * ratio);
            }

            if(height > maxHeight){
                ratio = (float)maxHeight / height;
                width = (int)(width * ratio);
                height = (int)(height * ratio);
            }

            viewHolder.image.setImageBitmap(Bitmap.createScaledBitmap(item.getDrawable(),width, height, false));


            viewHolder.base.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i = new Intent(context, Spud.class);
                    context.startActivity(i);
                }
            });
        }
        v.setTag(viewHolder);
        return v;
    }

    class ViewHolder{
        public TextView text;
        public TextView price;
        public ImageView image;
        public View base;
        public ViewHolder(View base){
            this.base = base;
            text = (TextView) base.findViewById(R.id.list_tv_text);
            price = (TextView) base.findViewById(R.id.list_tv_price);
            image = (ImageView) base.findViewById(R.id.list_iv_image);
        }
    }
}
