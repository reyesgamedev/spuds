package edu.spuds.sjsu.spudspotatobar;

import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import db.DatabaseHelper;


public class MainActivity extends AppCompatActivity {
    private Address address = new Address();
    private Button orderBtn;
    private SQLiteDatabase db;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitleTextColor(Color.WHITE);
        setSupportActionBar(toolbar);

        // set address
        address.setStreet("100 N. ALMADEN AVENUE");
        address.setBuildingNumber("STE #170");
        address.setState("CA");
        address.setCity("SAN JOSE");
        address.setZip("95110");

        TextView addressTxt = (TextView)findViewById(R.id.address_txt);
        addressTxt.setText(

                "100 N. ALMADEN AVENUE \n" +
                "STE #170, San Jose CA.\n"
        );


        // place this in another activity to allow for appropriate updating
        // also add a progress or waiting image to let the user know its updating;
        // also check that the user has internet

        DatabaseHelper db = new DatabaseHelper(this);
        //db.open();
        //String version = db.getVersion();
        db.download();
        db.close();



        // set Online Ordering Hours text
        TextView onlineOrderingHoursTxt = (TextView) findViewById(R.id.online_order_hours_txt);
        onlineOrderingHoursTxt.setText(R.string.online_ordering_hours);
        // set order button
        orderBtn = (Button) findViewById(R.id.order_button);
        orderBtn.setOnClickListener(orderHandler);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        if(id == R.id.shopping_cart){
            startActivity(new Intent(this, ShoppingCart.class));
        }
        return super.onOptionsItemSelected(item);
    }


    // order button Click listener
    private View.OnClickListener orderHandler = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            startActivity(new Intent(v.getContext(), SpudSelect.class));
        }
    };

}
